<?php

$enableFileContents = <<<TEXT
<?xml version="1.0" encoding="UTF-8"?>
<!-- https://phpunit.readthedocs.io/en/latest/configuration.html -->
<phpunit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="vendor/phpunit/phpunit/phpunit.xsd"
         colors="true"
         executionOrder="random"
         bootstrap="tests/bootstrap.php"
         cacheResult="true"
>
    <php>
        <ini name="error_reporting" value="-1"/>
        <env name="KERNEL_CLASS" value="Maklaro\MOSS\Kernel"/>
        <server name="SHELL_VERBOSITY" value="-1"/>
        <server name="SYMFONY_DEPRECATIONS_HELPER" value="max[direct]=0&amp;max[self]=0&amp;verbose=0"/>
    </php>

    <testsuites>
        <testsuite name="Project Test Suite">
            <directory>tests</directory>
        </testsuite>
    </testsuites>


    <!--    <coverage processUncoveredFiles="true">-->
    <!--        <include>-->
    <!--            <directory>./src</directory>-->
    <!--        </include>-->
    <!--        <exclude>-->
    <!--            <directory>./src/Application/Infrastructure/Migrations/</directory>-->
    <!--            <file>./src/Kernel.php</file>-->
    <!--        </exclude>-->
    <!--        <report>-->
    <!--            <html outputDirectory=".build-output/coverage"/>-->
    <!--            <text outputFile="php://stdout" showUncoveredFiles="false" showOnlySummary="true"/>-->
    <!--        </report>-->
    <!--    </coverage>-->

    <extensions>
        <extension class="DAMA\DoctrineTestBundle\PHPUnit\PHPUnitExtension"/>
        <extension class="Speicher210\FunctionalTestBundle\Extension\RestRequestFailTestExpectedOutputFileUpdater"/>
    </extensions>

    <listeners>
        <listener class="Symfony\Bridge\PhpUnit\SymfonyTestsListener"/>
    </listeners>
</phpunit>
TEXT;


file_put_contents('/home/evang522/code/maklaro/mossbe/phpunit.xml', $enableFileContents);
