# Utilities

function read_confirm
  while true
    read -l -P 'Do you want to continue? [y/N] ' confirm

    switch $confirm
      case Y y
        return 0
      case '' N n
        return 1
    end
  end
end



# RESET DB
function der --wraps='bin/console moss:dev:reset:env --no-interaction' --description 'Reset development environment'
    docker compose exec --user 1000:1000 php php bin/console moss:dev:reset:env --no-interaction $argv;
end

# Linting and Testing
alias pana "docker compose exec --user 1000:1000 php composer analyse"
alias stan "docker compose exec --user 1000:1000 php composer analyse-phpstan"
alias dep "docker compose exec --user 1000:1000 php composer analyse-deptrac"
alias psalm "docker compose exec --user 1000:1000 php php bin/console cache:warmup && docker-compose exec --user 1000:1000 php vendor/bin/psalm --no-cache --config=tools/Psalm/psalm.xml --show-info=false --threads=1"
alias psalm-u "docker compose exec --user 1000:1000 php vendor/bin/psalm --no-cache --config=tools/Psalm/psalm.xml --set-baseline=tools/Psalm/psalm.baseline.xml --threads=1"
alias pcheck "docker compose exec --user 1000:1000 php composer check" 
alias tsl "docker compose exec  --user 1000:1000 php vendor/bin/phpunit --exclude-group "large""
alias tpar "docker compose exec  --user 1000:1000 php composer test-parallel"
alias pta "docker compose exec  --user 1000:1000 php vendor/bin/phpunit"
alias tsls "docker compose exec  --user 1000:1000 php vendor/bin/phpunit --exclude-group "large" --stop-on-failure"
alias csc "docker compose exec  --user 1000:1000 php composer cs-c"
alias rmc "rm -rf ./var/cache && echo 'Cache removed ✅'"
alias as "echo 'Nope.'"
alias glo "git log --oneline"
alias ppp "git pull --rebase"
alias sss "git add . && git stash"


alias tp "docker compose exec  --user 1000:1000 php php ./test.php"

# Debug
alias paw "docker compose exec --user 1000:1000 php bin/console debug:autowiring" 
alias prout "docker compose exec --user 1000:1000 php bin/console debug:router" 

# Access Containers
alias pfish "docker compose exec --user 1000:1000 php fish"

# Service/container control
alias dup "docker compose up -d"
alias ddown "docker compose down"
alias dcr "docker compose restart"
alias dps "docker compose ps"

alias _fishconfig "code ~/.config/fish/"

alias a "git add -u"
alias s "git status"
alias grh "git reset --hard"

alias mb "git checkout develop"
alias gca "git commit --amend"
alias pb "git checkout -"
alias gg "git log --graph --oneline --decorate --all"
# remove dead branches. This is pretty buggy.
alias rmdb "echo 'git fetch -p ; git branch -r | awk \'{print $1}\' | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | awk \'{print $1}\' | xargs git branch -D'"



alias rmtl "rm -rf ./var/log/test.log && echo 'Test Log Removed ✅'"
alias dmtl "rm -rf ./var/log/dev.log && echo 'Dev Log Removed ✅'"
alias tlce "cat ./var/log/test.log | grep \".CRITICAL\""
alias dlce "cat ./var/log/dev.log | grep \".CRITICAL\""
alias dl "cat ./var/log/dev.log"



# utilities
alias _fuelinfo "php ~/code/personal/command/bin/console household:fuel:get-local-diesel-prices"
alias news "php ~/code/personal/command/bin/console news:headlines"
alias _kathnews "php ~/code/personal/command/bin/console news:kathnet"
alias tagespost "php ~/code/personal/command/bin/console news:tagespost"
alias tr "php ~/code/personal/command/bin/console translate"
alias r "ranger"
alias passgen "openssl rand -hex 12"

alias cmd "php ~/code/personal/command/bin/console"
alias _fire "echo 'https://www.youtube.com/watch?v=B8If3PmUHLI'"
alias _forest "echo 'https://www.youtube.com/watch?v=oN-RTT8rVY0&list=PLsyEJt4jPElVeFxr8Aex6vx2n3h5OR1ee&index=26'"
alias _night "echo 'https://www.youtube.com/watch?v=xsfyb1pStdw'"
alias _meditate "echo 'https://www.youtube.com/watch?v=tIZSN4T-Mys'"
alias _medieval "echo 'https://youtu.be/5F5dgg1eeGE'"
alias books "code ~/code/personal/my_books/"
alias word "cmd language:word-info"



alias todo "cmd todo"

# System
alias _update "sudo apt update && sudo apt upgrade -y && sudo apt autoremove"

alias pts "pt tests/UI/Application/Api/OpenApi/SchemaController/SchemaControllerTest.php && pt tests/UI/Application/Admin/Api/OpenApi/SchemaController/SchemaControllerTest.php"

alias deps "docker compose exec --user 1000:1000 php composer install"
alias books "cmd research:books:get-recommended-books"
alias ai "cmd research:chat:start-session"



# ---------- FUNCTIONS ------------------------------------------->

function services
        echo "Expose text"
        echo "Grundbuchauszug  (Land Registration)"
        echo "Baulastenverzeichnis (property encumbrances)"
        echo "Altlastenverzeichnis (contaminated sites)"
        echo "Amtliche Flurkarte (cadastral map)"
        echo "Nicht amtliche flurkarte (non-official cadastral map)"
        echo "Grundriss (Floor plan)"
end


function cmigration --argument-names hash
        git show $hash --name-only | grep 'Version'
end

# Update Fish config
function _updateLocalFishConfig --description 'Update Fish Config File'
        git clone https://gitlab.com/evang522/fishconfig.git ~/fishshortcuts
        cd ~/fishshortcuts
        
        cp ./config.fish ~/.config/fish/config.fish
        cp -R ./utilities ~/.config/fish/
        cd ..
        rm -rf ~/fishshortcuts
        echo 'Removed FishConfig Repository'
end

# Update Fish config
function _pushNewFishConfig --description 'Update Fish Config File'
        git clone https://gitlab.com/evang522/fishconfig.git ~/fishshortcuts
        cd ~/fishshortcuts
        
        cp ~/.config/fish/config.fish ./config.fish 
        cp -R ~/.config/fish/utilities .
        cat ~/.config/fish/config.fish
        git add .
        git commit -m 'Updated Fish Config With Script'
        git push
        cd ..
        rm -rf ~/fishshortcuts
        echo 'Removed FishConfig Repository'
end

function ptg --description 'Run PHP test unit with a group' --argument-names groupName
        docker-compose exec --user 1000:1000 php composer test -- --group $groupName;
end

function fcs --description 'Search in Fish Config' --argument-names searchTerm
        cat ~/.config/fish/config.fish | grep "$searchTerm"
end

function pt --description 'Run PHP test unit with specified file path' --argument-names filePath
        set search_string 'src'
        set replace_string 'tests'
        set ending_search '.php'
        set test_ending 'Test.php'


        set initial_filepath (echo $filePath | sed "s/$search_string/$replace_string/g")
        
        if string match -q -- '*Test.php' $initial_filepath
                set target_filepath $initial_filepath
        else
                set target_filepath (echo $initial_filepath | sed "s/$ending_search/$test_ending/g")
        end
        
        docker-compose exec --user 1000:1000 php composer test $target_filepath;
end



function ptt --description 'Run PHP test unit with specified file path for group ttt' --argument-names filePath
        docker-compose exec --user 1000:1000 php composer test $filePath -- --group ttt;
end


function jt --description 'Run Jest test unit with specified file path' --argument-names filePath
        yarn jest $filePath --watchAll
end


function ptg --description 'Run PHP test unit with specified file path and test group' --argument-names filePath groupName
        docker-compose exec --user 1000:1000 php composer test $filePath -- --group $groupName;
end

function execphp --description 'Run PHP test unit with a group' --argument-names cmd
        docker-compose exec --user 1000:1000 php $cmd;
end

function pbc --description 'Run bin/console command in the php container' --argument-names cmd
        docker-compose exec --user 1000:1000 php php bin/console $cmd;
end

function dlogs --description 'Check the docker compose logs for a specific container' --argument-names cmd
        docker-compose logs $cmd;
end

function gcm --description 'Git commit with a provided message' --argument-names msg
        git commit -m $msg;
end

function gc --description 'Git Checkout branch' --argument-names branch
        git checkout $branch;
end

function moss:esu
        php "/home/evang522/.config/fish/utilities/moss-esu/index.php"
end

function moss:dsu
        php "/home/evang522/.config/fish/utilities/moss-dsu/index.php"
end

function moss:e:esu
        php "/home/evang522/.config/fish/utilities/moss-email-esu/index.php"
end

function moss:e:dsu
        php "/home/evang522/.config/fish/utilities/moss-email-dsu/index.php"
end

function moss:jt:esu
        php "/home/evang522/.config/fish/utilities/moss-json-type-test-esu/index.php"
end

function moss:jt:dsu
        php "/home/evang522/.config/fish/utilities/moss-json-type-test-dsu/index.php"
end

function moss:cmd:esu
        php "/home/evang522/.config/fish/utilities/moss-command-test-esu/index.php"
end

function moss:cmd:dsu
        php "/home/evang522/.config/fish/utilities/moss-command-test-dsu/index.php"
end

# run cs-fix on file or entire codebase
function csf  --argument-names file
        # checks if file var is set, if so, fixes specific file. 
        if test -n "$file"
                docker-compose exec --user 1000:1000 php vendor/bin/phpcbf -p -d file $file
        else 
                docker-compose exec  --user 1000:1000 php composer cs-f
        end
end

# Project links
alias apidoc "echo 'https://moss.local/api/doc'"


# compare the commits differing between two git branches 
function gcb  --argument-names branch1 branch2
        git log --oneline --left-right --cherry-pick $branch1...$branch2
end



# Git functions
function grho 
         if read_confirm
                git reset --hard origin/(git branch --show-current)
        else
                echo "Git reset hard to origin cancelled"
        end
end

function gpf 
        if read_confirm
                git push -f
        else
                echo "Git push -f cancelled"
        end
end


function gsc  --argument-names searchTerm
        git log -S $searchTerm -p | grep $searchTerm -C 4 
end

function notes  --argument-names name
        if test -n "$name"
                cat ~/code/personal/notes/$name.md
        else
                cat ~/code/personal/notes/note.md
        end
end

function _notes  --argument-names name
        if test -n "$name"
                nano ~/code/personal/notes/$name.md
        else
                nano ~/code/personal/notes/note.md
        end
end

function vnote  --argument-names name
        if test -n "$name"
                pandoc --standalone ~/code/personal/notes/$name.md -o /tmp/test.html && open /tmp/test.html
        else
                pandoc --standalone ~/code/personal/notes/note.md -o /tmp/test.html && open /tmp/test.html
        end
end

function ssht  --argument-names command
	ssh home.server -t "$command"
end


alias lnotes "ls ~/code/personal/notes | sed 's/.md/ /g'" 

function dnote  --argument-names name
        rm -rf  ~/code/personal/notes/$name.md
end

alias quality "cat /home/evang522/.config/JetBrains/PhpStorm2023.1/scratches/maklaro/codequality.md"

alias riml "git checkout ./.idea/moss.iml"

alias extensions "cd ~/.local/share/gnome-shell/extensions/"


alias openshot "~/code/personal/OpenShot-DONT-DELETE.AppImage"

alias litterscooped "cmd household:litter:set-scooped"
alias mprod "echo https://moss.maklaro.com/admin"
alias mstaging "echo https://moss-staging.maklaro.com/admin"
alias mdev "echo https://moss.local/admin"

alias dcmd "docker-compose exec --user 1000:1000 php php bin/console"
alias n:pdf "cmd research:notes:output-to-pdf"

# bun
set --export BUN_INSTALL "$HOME/.bun"
set --export PATH $BUN_INSTALL/bin $PATH



